/*Bibliothèques*/
console.log('Démarrage du serveur....');
var express = require('express');
var multer = require('multer');
var formidable = require('formidable');
var fs = require('fs');

var destination = multer.diskStorage({
  destination: 'temp_data/',
  filename: function (req, file, cb) {
    cb(null, Date.now()+file.originalname)
  }
})

const upload = multer({storage: destination})

var app = express();

console.log('Initialisation....');
app.use(express.static('assets/'));
app.listen(8080, console.log('Serveur démarré avec succès sur le port 8080'));

app.get('/c', function (req, res) {
  var reponse = {"status":"ok"};
  let reponseText = JSON.stringify(reponse);
  console.log("check");
  res.writeHead(200);
	res.end(reponseText);
}); 

app.get('/putImage', function (req, res) {
  var reponse = {"status":"-1","message":"Synthase incorrecte"};
  let reponseText = JSON.stringify(reponse);
  console.log("putImage via GET");
  res.writeHead(200);
	res.end(reponseText);
});

app.post('/putImage', upload.single('file'), function(req, res) {
  var fichier = '../serveur/'+req.file.path;
  console.log("Détéction : "+fichier);
  const { spawn } = require('child_process');
  const pyProg = spawn('python', ['../IA/predict.py',  '--graph=/tmp/output_graph.pb', '--labels=/tmp/output_labels.txt', '--input_layer=Placeholder', '--output_layer=final_result', '--image='+fichier]);

  pyProg.stdout.on('data', function(data) {
    var reponse = data.toString().trim();
    var obj = JSON.parse(reponse);
    console.log("");
    console.log("Produits détectés")
    for (var i = obj.results.length - 1; i >= 0; i--) {
      console.log("\t"+obj.results[i].information.title+" ===> "+obj.results[i].information.pourcentage);
    }
    console.log("");
    console.log("Produits similaires")
    for (var i = obj.similar.length - 1; i >= 0; i--) {
      console.log("\t"+obj.similar[i].information.title+" ===> "+obj.similar[i].information.pourcentage);
    }

    console.log("");
    console.log("Autres Produits")
    for (var i = obj.similar.length - 1; i >= 0; i--) {
      console.log("\t"+obj.others[i].information.title+" ===> "+obj.others[i].information.pourcentage);
    }
    console.log("");

    res.writeHead(200);
	  res.end(reponse);
    console.log("Détection terminée");
  });

  pyProg.stderr.on('data', function(data) {
    var reponse = {"status":"-1","message":"Erreur survenue dans la détection"};
    let reponseText = JSON.stringify(reponse);
    console.log("Erreur survenue dans la détection");
    res.writeHead(200);
    res.end(reponseText);
  });

 });

/*404*/
app.use(function(req, res) {
  console.log("Requete inconnue");
  var reponse = {"status":"-1","message":"Requete inconnue"};
  let reponseText = JSON.stringify(reponse);
  res.writeHead(200);
	res.end(reponseText);
});

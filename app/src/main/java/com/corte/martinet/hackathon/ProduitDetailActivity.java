package com.corte.martinet.hackathon;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.corte.martinet.hackathon.Model.ProductDataModel;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

public class ProduitDetailActivity extends AppCompatActivity {


    private ImageView ivImage;
    private TextView tvTitle;
    private TextView tvPrice;
    private TextView tvDesc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_produit_detail);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ProductDataModel produit = (ProductDataModel) getIntent().getSerializableExtra("DATAS");
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ivImage = findViewById(R.id.ivImage);
        tvTitle = findViewById(R.id.tvTitle);
        tvPrice = findViewById(R.id.tvPrice);
        tvDesc = findViewById(R.id.tvDesc);


        Picasso.get().load(produit.getImage()).into(ivImage);
        tvTitle.setText(produit.getTitle());
        tvPrice.setText(produit.getPrice()+" €");
        tvDesc.setText(produit.getDescription());

    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return false;
    }


}

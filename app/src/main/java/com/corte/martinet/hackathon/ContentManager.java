package com.corte.martinet.hackathon;

import android.annotation.SuppressLint;
import android.os.AsyncTask;

import com.corte.martinet.hackathon.Model.ProductDataModel;
import com.corte.martinet.hackathon.ViewModel.ProductViewModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class ContentManager
{
    private static ContentManager mInstance;

    public static ContentManager getInstance()
    {
        if (mInstance == null)
            mInstance = new ContentManager();

        return mInstance;
    }

    public static List<ProductDataModel> retrieveProductsFromJson(String jsonString)
    {
        List<ProductDataModel> placesList = new ArrayList<>();
        try
        {
            JSONObject jsonPlaces = new JSONObject(jsonString);
            JSONArray placesArray = jsonPlaces.getJSONArray("results");
            int nbPlaces = placesArray.length();
            for (int i = 0; i < nbPlaces; i++)
            {
                placesList.add(new ProductDataModel(placesArray.getJSONObject(i)));
                System.out.println(new ProductDataModel(placesArray.getJSONObject(i)));
            }
            placesArray = jsonPlaces.getJSONArray("similar");
            nbPlaces = placesArray.length();
            for (int i = 0; i < nbPlaces; i++)
            {
                ProductDataModel productDataModel = new ProductDataModel(placesArray.getJSONObject(i));
                //if(placesList.contains(productDataModel)) {
                    placesList.add(productDataModel);
                //}
            }
            Collections.sort(placesList, (o1, o2) -> o1.getPoucentage() < o2.getPoucentage() ? 1 : -1);

        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
        return placesList;
    }

    private ContentManager() {}



}

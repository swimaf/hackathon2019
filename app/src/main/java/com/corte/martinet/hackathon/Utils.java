package com.corte.martinet.hackathon;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.Charset;

public class Utils
{
    // PUT IN GRADLE THIS DEPENDENCY
    // implementation "commons-io:commons-io:+"

    public static String getTextFromStream(InputStream is)
    {
        try
        {
            StringWriter writer = new StringWriter();
            IOUtils.copy(is, writer, Charset.forName("UTF-8"));
            return writer.toString();
        }
        catch (Exception e)
        {
            return null;
        }
    }

    public static boolean isStringValid(String string){
        return string != null && !string.isEmpty();
    }


    public static Bitmap getImage(String fileName){
        byte[] imageData = null;

        try
        {

            final int THUMBNAIL_SIZE = 64;

            FileInputStream fis = new FileInputStream(fileName);
            Bitmap imageBitmap = BitmapFactory.decodeStream(fis);


            return imageBitmap;
        }
        catch(Exception ex) {

        }
        return null;
    }


    public static String getBasePath() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "UltimateCameraGuideApp");

        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.e("Camera Guide", "Required media storage does not exist");
                return null;
            }
        }
        return mediaStorageDir.getPath();
    }
    public static String getRealPathFrom(Context context, Uri uri) {

        Log.i("uri", uri.getPath());
        String filePath = "";
        if (uri.getScheme().equals("file")) {
            return uri.getPath();
        } else if (DocumentsContract.isDocumentUri(context, uri)) {
            String wholeID = DocumentsContract.getDocumentId(uri);
            Log.i("wholeID", wholeID);
            // Split at colon, use second item in the array
            String[] splits = wholeID.split(":");
            if (splits.length == 2) {
                String id = splits[1];

                String[] column = {MediaStore.Images.Media.DATA};
                // where id is equal to
                String sel = MediaStore.Images.Media._ID + "=?";
                Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        column, sel, new String[]{id}, null);
                int columnIndex = cursor.getColumnIndex(column[0]);
                if (cursor.moveToFirst()) {
                    filePath = cursor.getString(columnIndex);
                }
                cursor.close();
            }
        }
        return filePath;
    }
}

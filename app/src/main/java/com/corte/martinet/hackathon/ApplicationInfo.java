package com.corte.martinet.hackathon;

import android.app.Application;
import android.content.Context;

public class ApplicationInfo extends Application {

    public static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }

}
package com.corte.martinet.hackathon;

import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.corte.martinet.hackathon.Api.ApiService;
import com.corte.martinet.hackathon.Model.ProductDataModel;
import com.corte.martinet.hackathon.ViewModel.ProductViewModel;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class SearchActivity extends AppCompatActivity {

    private AdapterHome adapterHome;
    private ProductViewModel mViewModel;
    private static final String UPLOAD_URL = "http://172.20.10.126:8080/";
    private ApiService apiService;
    private LinearLayout chargement;
    private LinearLayout erreur;
    private RecyclerView recyclerView;
    private File file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.BLACK);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        this.file = (File) getIntent().getSerializableExtra("file");
        System.out.println(file);
        this.adapterHome = new AdapterHome(this, new ArrayList<>());
        this.chargement = findViewById(R.id.loading);
        this.erreur = findViewById(R.id.erreur);
        recyclerView = findViewById(R.id.recycler);
        recyclerView.setAdapter(adapterHome);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);


        mViewModel = ViewModelProviders.of(this).get(ProductViewModel.class);
        mViewModel.getPlaceDataModels().observe(this, data -> {
            if (data != null) bindPlacesToUI(data);
        });

        initRetrofitClient();

        multipartImageUpload();

    }

    private void bindPlacesToUI(List<ProductDataModel> productDataModels) {
        adapterHome.setPlaces(productDataModels);
        adapterHome.notifyDataSetChanged();
    }


    public void close() {
        finish();
        overridePendingTransition(R.anim.slide_out,R.anim.slide_close);
    }

    @Override
    public boolean onSupportNavigateUp() {
        close();
        return false;
    }

    @Override
    public void onBackPressed() {
        close();
        super.onBackPressed();
    }

    private void initRetrofitClient() {
        OkHttpClient client = new OkHttpClient.Builder()
                .retryOnConnectionFailure(true)
                .readTimeout(20, TimeUnit.SECONDS)
                .connectTimeout(20, TimeUnit.SECONDS)
                .build();

        apiService = new Retrofit.Builder()
                .baseUrl(UPLOAD_URL)
                .client(client)
                .build()
                .create(ApiService.class);
    }

    private void multipartImageUpload() {

        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), reqFile);
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), "upload");
        Call<ResponseBody> req = apiService.postImage(body, name);
        req.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.code() == 200) {
                    //textView.setText("Uploaded Successfully!");
                    //textView.setTextColor(Color.BLUE);
                    try {
                        bindPlacesToUI(ContentManager.retrieveProductsFromJson(response.body().string()));
                        chargement.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        System.out.println(response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    chargement.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.GONE);
                    erreur.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                chargement.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
                erreur.setVisibility(View.VISIBLE);
            }
        });


    }


    public void learnImage() {
        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), reqFile);
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), "upload");
        Call<ResponseBody> req = apiService.learnImage(body, name);
        req.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.code() == 200) {
                    Toast.makeText(getApplicationContext(), "Image entregistré", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Erreur entregistré", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Erreur entregistré", Toast.LENGTH_LONG).show();
            }
        });


    }

}

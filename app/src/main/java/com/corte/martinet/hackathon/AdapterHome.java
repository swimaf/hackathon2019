package com.corte.martinet.hackathon;


import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.corte.martinet.hackathon.Model.ProductDataModel;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AdapterHome extends RecyclerView.Adapter<AdapterHome.AdapterViewHolder> {

    private List<ProductDataModel> place;
    private SearchActivity mContext;

    public AdapterHome(SearchActivity context, ArrayList<ProductDataModel> places) {
        mContext = context;
        this.place = places;
    }


    @NonNull
    @Override
    public AdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_row, parent, false);
        return new AdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterViewHolder adapterViewHolder, int i) {
        adapterViewHolder.bindPlace(place.get(i));
        adapterViewHolder.itemView.setOnClickListener(v -> {
            Intent intent = new Intent(ApplicationInfo.context, ProduitDetailActivity.class);
            intent.putExtra("DATAS", place.get(i));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            ApplicationInfo.context.startActivity(intent);
        });
        adapterViewHolder.btnValid.setOnClickListener(v -> {
            new AlertDialog.Builder(new ContextThemeWrapper(mContext, R.style.myDialog))
                    .setMessage("Voulez-vous valider la photo pour entrainer le modèle ?")
                    .setPositiveButton("OK", (dialog, which) -> {
                        adapterViewHolder.btnValid.setVisibility(View.GONE);
                        mContext.learnImage();
                    })
                    .setNegativeButton("Cancel", null)
                    .create()
                    .show();
        });
    }

    @Override
    public int getItemCount() {
        return place.size();
    }

    public void setPlaces(List<ProductDataModel> placesList) {
        this.place = placesList;
    }

    public class AdapterViewHolder extends RecyclerView.ViewHolder {

        private Context mContext;
        private TextView tvTitle;
        private TextView tvDescription;
        private ImageView tvImage;
        private TextView tvPrice;
        private TextView tvResem;
        private ImageButton btnValid;

        public AdapterViewHolder(View itemView) {
            super(itemView);
            mContext = itemView.getContext();
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvDescription = itemView.findViewById(R.id.tvDescription);
            tvImage = itemView.findViewById(R.id.tvImage);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            btnValid = itemView.findViewById(R.id.btnValid);
            tvResem = itemView.findViewById(R.id.tvRessemblance);
        }


        public void bindPlace(ProductDataModel productDataModel) {
            tvTitle.setText(productDataModel.getTitle());
            tvDescription.setText(productDataModel.getDescription());
            tvPrice.setText(productDataModel.getPrice()+" €");
            tvResem.setText(productDataModel.getPoucentage()+"%");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                tvResem.setTooltipText("Pourcentage de ressemblance");
            }
            Picasso.get().load(productDataModel.getImage()).into(tvImage);
        }
    }


}
package com.corte.martinet.hackathon.ViewModel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.corte.martinet.hackathon.ApplicationInfo;
import com.corte.martinet.hackathon.ContentManager;
import com.corte.martinet.hackathon.Model.ProductDataModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ProductViewModel extends ViewModel {
    private MutableLiveData<List<ProductDataModel>> placeDataModels;

    public MutableLiveData<List<ProductDataModel>> getPlaceDataModels() {
        if(placeDataModels == null) {
            placeDataModels = new MutableLiveData<>();
            setPlaceDataModels(new ArrayList<>());
        }
        return placeDataModels;
    }

    public void setPlaceDataModels(List<ProductDataModel> productDataModels) {
        this.placeDataModels.setValue(productDataModels);
    }
}

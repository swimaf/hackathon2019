package com.corte.martinet.hackathon.Model;

import org.json.JSONObject;

import java.io.Serializable;
import java.text.DecimalFormat;

public class ProductDataModel implements Serializable
{

    private Integer id;
    private String title;
    private double  price;
    private String  description;
    private String image;
    private Integer poucentage;



    public static final String ID     = "id";
    public static final String TITLE     = "title";
    public static final String PRICE = "price";
    public static final String DESCRIPTION       = "summary";
    public static final String IMAGE     = "image_file";
    public static final String POURCENTAGE     = "pourcentage";


    public ProductDataModel(JSONObject o)
    {
        JSONObject e = o.optJSONObject("information");
        this.title = e.optString(TITLE, "");
        this.description = e.optString(DESCRIPTION, "");
        this.price = e.optDouble(PRICE, 0.0);
        this.image = e.optString(IMAGE, "");
        this.poucentage = e.optInt(POURCENTAGE, 0);
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public double getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public String getImage() {
        return image;
    }

    public Integer getPoucentage() {
        return poucentage;
    }

    @Override
    public String toString() {
        return "ProductDataModel{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", price=" + price +
                ", description='" + description + '\'' +
                ", image='" + image + '\'' +
                ", poucentage=" + poucentage +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        ProductDataModel productDataModel = (ProductDataModel) obj;
        return productDataModel.title.equals(this.title);
    }
}
